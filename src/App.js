import React, { Component } from 'react';
import logo from './logo.svg';
import Listing from './components/listing';
import Main from './components/landing';
import Features from './components/features';
import './App.css';

class App extends Component {
  render() {
    return (
       <div>
          <Main/>
          <Features/>
          <Listing/>
       </div>
    );
  }
}

export default App;
