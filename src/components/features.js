import React, { Component } from 'react';
import '../App.css';

export default class Features extends Component {
	render() {
		return (
			<div className="our-features">
				<div className="container">
					<div className="row row-features">
						<div className="col-md-4">
							<p>
								<i className="fa fa-check-circle" aria-hidden="true"></i>
								Adult dating &amp; hookup site
							</p>
						</div>

						<div className="col-md-4">
							<p>
								<i className="fa fa-check-circle" aria-hidden="true"></i>
								Flirt with local dating contacts
							</p>
						</div>

						<div className="col-md-4">
							<p>
								<i className="fa fa-check-circle" aria-hidden="true"></i>
								Female friendly adult dating
							</p>
						</div>
					</div>
				</div>
			</div>
		);
	}
}