import React, { Component } from 'react';
import placeHolderImg from '../img/user-img.png';
import '../App.css';

export default class Listing extends Component {
	render() {
		return (
			<div className="listing-section">
				<div className="container">
					<div className="row views-row">
						<div className="col-md-3 col-xs-6 col-user-img">
							<div className="user-img-container">
								<img src={placeHolderImg} alt="user-img" className="img-responsive"/>
							</div>
						</div>
						<div className="col-md-9 col-xs-6">
							<div className="user-identity">
								<ul>
									<li>
										<h3>Teni | +234567890</h3>
										<span className="user-location">
                                    Lagos
                                </span>
									</li>
									<li className="pull-right">
										<span className="user-preference">Straight Dating</span>
									</li>
								</ul>
							</div>
							<div className="user-story">
								<p>Nice and nice 35a. I’m waiting for gentlemen. SMS and hidden numbers - do not answer! Hi, I am a very sexy and friendly girl. Call me. Hey, I’m a very sexy guy and a great girlfriend. Tel. +37253852622.Hi, I am a very sexy and friendly girl. Call me. Hey, I’m a very sexy guy and a great girlfriend. Tel. +37253852622.</p>
							</div>
							<div className="view-listing">
								<a href="#">View Listing <i className="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>

					<div className="row views-row">
						<div className="col-md-3 col-xs-6 col-user-img">
							<div className="user-img-container">
								<img src={placeHolderImg} alt="user-img" className="img-responsive"/>
							</div>
						</div>
						<div className="col-md-9 col-xs-6">
							<div className="user-identity">
								<ul>
									<li>
										<h3>Teni | +234567890</h3>
										<span className="user-location">
                                    Lagos
                                </span>
									</li>
									<li className="pull-right">
										<span className="user-preference">Straight Dating</span>
									</li>
								</ul>
							</div>
							<div className="user-story">
								<p>Nice and nice 35a. I’m waiting for gentlemen. SMS and hidden numbers - do not answer! Hi, I am a very sexy and friendly girl. Call me. Hey, I’m a very sexy guy and a great girlfriend. Tel. +37253852622.Hi, I am a very sexy and friendly girl. Call me. Hey, I’m a very sexy guy and a great girlfriend. Tel. +37253852622.</p>
							</div>
							<div className="view-listing">
								<a href="#">View Listing <i className="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>

					<div className="row views-row">
						<div className="col-md-3 col-xs-6 col-user-img">
							<div className="user-img-container">
								<img src={placeHolderImg} alt="user-img" className="img-responsive"/>
							</div>
						</div>
						<div className="col-md-9 col-xs-6">
							<div className="user-identity">
								<ul>
									<li>
										<h3>Teni | +234567890</h3>
										<span className="user-location">
                                    Lagos
                                </span>
									</li>
									<li className="pull-right">
										<span className="user-preference">Straight Dating</span>
									</li>
								</ul>
							</div>
							<div className="user-story">
								<p>Nice and nice 35a. I’m waiting for gentlemen. SMS and hidden numbers - do not answer! Hi, I am a very sexy and friendly girl. Call me. Hey, I’m a very sexy guy and a great girlfriend. Tel. +37253852622.Hi, I am a very sexy and friendly girl. Call me. Hey, I’m a very sexy guy and a great girlfriend. Tel. +37253852622.</p>
							</div>
							<div className="view-listing">
								<a href="#">View Listing <i className="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>

					<div className="row views-row">
						<div className="col-md-3 col-xs-6 col-user-img">
							<div className="user-img-container">
								<img src={placeHolderImg} alt="user-img" className="img-responsive"/>
							</div>
						</div>
						<div className="col-md-9 col-xs-6">
							<div className="user-identity">
								<ul>
									<li>
										<h3>Teni | +234567890</h3>
										<span className="user-location">
                                    Lagos
                                </span>
									</li>
									<li className="pull-right">
										<span className="user-preference">Straight Dating</span>
									</li>
								</ul>
							</div>
							<div className="user-story">
								<p>Nice and nice 35a. I’m waiting for gentlemen. SMS and hidden numbers - do not answer! Hi, I am a very sexy and friendly girl. Call me. Hey, I’m a very sexy guy and a great girlfriend. Tel. +37253852622.Hi, I am a very sexy and friendly girl. Call me. Hey, I’m a very sexy guy and a great girlfriend. Tel. +37253852622.</p>
							</div>
							<div className="view-listing">
								<a href="#">View Listing <i className="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>

					<nav className="pager" aria-label="Page navigation">
						<ul className="pagination list-pager">
							<li className="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li>
								<a href="#" aria-label="Next">Next
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		);
	}
}



