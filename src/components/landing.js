import React, { Component } from 'react';
import '../App.css';

export default class Main extends Component {
	render() {
		return (
			<div className="jumbotron">
				<div className="hero-bg"></div>
				<div className="container">
					<div className="hero-content">
						<div className="row">
							<div className="col-md-6 hero-contentLeft wow fadeInUp" data-wow-duration="2s">
								<h1>Lagos Sex Date<br/>
									<span>Join free the Best Swingers Website</span>
								</h1>
								<p>You must be 18 years of age or older to use this website. If you are younger than 18 years of age you must leave this page now.</p>

								<div className="call-btn">
									<button className="btn btn-apply">Get started today</button>

									<button className="btn btn-rates">  Already registered? Sign in</button>
								</div>
							</div>

							<div className="col-md-6 hero-contentRight  wow fadeInUp" data-wow-duration="2s">
								<div className="request-form">
									<div className="form-header">
										<h2>Just take 2 minutes to join the fun</h2>
										<p>Fill up the form below and get an exclusive invite to the best time of your adult sex life.</p>
									</div>

									<hr className="form-line"/>
										<form>
											<div className="form-group">
												<label className="control-label sr-only" for="name">Name</label>
												<input type="text" placeholder="Name" className="form-control input-md" required/>
											</div>

											<div className="form-group">
												<label className="control-label sr-only" for="name">Email</label>
												<input type="email" placeholder="E-mail" className="form-control input-md" required/>
											</div>

											<div className="form-group">
												<label className="control-label sr-only" for="name">Password</label>
												<input type="password" placeholder="Password number" className="form-control input-md" required/>
											</div>

											<div className="form-group">
												<label className="control-label sr-only" for="name">Security Question</label>
												<input type="text" placeholder="How much is ten minus four?" className="form-control input-md" required/>
											</div>
											<button className="btn btn-apply btn-large">REGISTER</button>
										</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		);
	}
}

